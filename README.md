# PostgreSQL role

Роль устанавливает PostgreSQL сервер и поднимает асинхронную hot standby реплику.
Создаёт пользователей, базы данных и даёт полные привилегии на них на основании содержимого postgresql_service_users.

Создаёт RBAC-style роли для организации доступа только на SELECT или ARWD(Append/Read/Write/Delete) пользователям не являющимся владельцами баз данных. Подробнее об устройстве такой схемы можно узнать в [Организация ролей и групп доступа PostgreSQL](#)

- работает с Ansible >= 2.8

Поддерживаемые версии PostgreSQL:

- 9
- 10
- 11
- 12

## Конфигурация

Настройки кластера postgres по-умолчанию расположены в [defaults/main.yml](#)

```
# конфигурационный файл postgresql.conf генерируется из словаря
postgresql_global_config_options:
  max_connections: 60
  shared_buffers: 32GB
  huge_pages: "try"
  temp_buffers: 32MB
  work_mem: 11983kB
  maintenance_work_mem: 2GB
  effective_cache_size: 96GB

postgresql_cluster_if: em2 # интерфейсы серверов через которые работает репликация, нам нужна внутренняя сеть

# роли и базы данных создаются из словаря, выдаются полные права на базы сервисным ролям
postgresql_service_users:
  svc1: password
  svc2: password
  svc3: password
  svc4: password

# роли с доступом на чтение
postgresql_ro_users:
  support: supppass

# роли с доступом на SELECT/INSERT/UPDATE/DELETE
postgresql_arwd_users:
  somedev: somepass

```

- postgresql_global_config_options - остаётся в параметрах по-умолчанию (если не нужно переопределить размеры буферов/количество подключений/настройки автовакуума и прочие параметры). При необходимости внесения изменений - выносим в хостовые или групповые переменные
- postgresql_service_users переопределяются для мастера в групповых переменных в инвентори

В инвентори hosts.yml должны быть группы с префиксом pg\_:

```
❯ cat inventory/eu-test/hosts.yml
---
all:
  children:
    postgres:
      children:
        pg_master:
          hosts:
            db-01.int.dev: {}
        pg_replica:
          hosts:
            db-02.int.dev: {}
```

для активации реплики в групповые переменные postgres.yml необходимо добавить:

```
postgresql_hot_standby: true
postgresql_repl_user: yyy
postgresql_repl_pass: xxx
```

Встроена проверка необходимости переналить реплику. Реплика будет переналита если не обнаружен соответствующий процесс postgres на сервере

```
- name: PostgreSQL replica | Check if replication already works
  shell: "ps ax | grep '[p]ostgres: startup.*' > /dev/null && echo REPLICA || echo BASEBACKUP"
  register: pgstate
  when: inventory_hostname in groups['pg_replica']
  changed_when: pgstate.stdout != "REPLICA"
```
