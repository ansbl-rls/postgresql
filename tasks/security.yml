---
#################### Give us security #########################
# revoke all privileges on public schema in template1 database
# REVOKE CONNECT ON database "template1" FROM PUBLIC;
- name: PostgreSQL | Revoke CONNECT from PUBLIC on template1
  postgresql_privs:
    db: template1
    type: database
    state: absent
    priv: CONNECT
    role: PUBLIC
  become: true
  become_user: "{{ postgresql_user }}"
# REVOKE ALL ON schema "public" FROM PUBLIC;
- name: PostgreSQL | Revoke ALL from PUBLIC on template1
  postgresql_privs:
    db: template1
    objs: public
    type: schema
    state: absent
    priv: ALL
    role: PUBLIC
  become: true
  become_user: "{{ postgresql_user }}"

############### create service roles, dbs, privs #############
# generate lists of dictionaries with service users(roles),
# databases, privileges from postgresql_service_users dict
- name: PostgreSQL | Generate service users list
  set_fact:
    # this list permit LOGIN to servrices users
    postgresql_svc_users_list: >
      {{ postgresql_svc_users_list|
      default([]) +
      [{'name': item.key, 'password': item.value, 'priv': 'ALL', 'role_attr_flags': 'LOGIN' }] }}
    # this list set owners for services databases
    postgresql_db_list: >
      {{ postgresql_db_list|
      default([]) +
      [{'name': item.key, 'owner': item.key }] }}
    # this list grant all privileges to services users
    postgresql_svc_privileges_list: >
      {{ postgresql_svc_privileges_list|
      default([]) +
      [{'name': item.key, 'objs': item.key, 'type': 'database', 'privs': 'ALL' }] +
      [{'name': item.key, 'db': item.key, 'type': 'schema', 'objs': 'public', 'privs': 'ALL' }] }}
    # generate read only r_ prefixed roles list
    postgresql_ror_list: >
      {{ postgresql_ror_list|
      default([]) +
      [{'name': 'r_'+item.key+'_ro', 'role_attr_flags':
      'NOSUPERUSER,INHERIT,NOCREATEDB,NOCREATEROLE,NOREPLICATION,NOLOGIN',
      'db': item.key, 'objs': 'public', 'type': 'schema', 'privs': 'USAGE'}] }}
    # this list grant SELECT to read only r_ prefixed roles
    postgresql_default_ror_list: >
      {{ postgresql_default_ror_list|
      default([]) + 
      [{'name': 'r_'+item.key+'_ro', 'db': item.key, 'objs': 'TABLES,SEQUENCES',
      'privs': 'SELECT', 'type': 'default_privs', 'target_roles': item.key }] }}
    # generate read only g_ prefixed roles list
    postgresql_rog_user_list: >
      {{ postgresql_rog_user_list|
      default([]) +
      [{'name': 'g_'+item.key+'_ro', 'role_attr_flags':
      'NOSUPERUSER,INHERIT,NOCREATEDB,NOCREATEROLE,NOREPLICATION,NOLOGIN'}] }}
    # this list grant r_ prefixed privileges to g_ prefixed role
    postgresql_rog_privs_list: >
      {{ postgresql_rog_privs_list|
      default([]) +
      [{'db': item.key, 'objs': 'r_'+item.key+'_ro', 'name': 'g_'+item.key+'_ro', 'type': 'group'}] }}
    # generate SELECT/INSERT/UPDATE/DELETE r_ prefixed roles list
    postgresql_arwdr_list: >
      {{ postgresql_arwdr_list|
      default([]) +
      [{'name': 'r_'+item.key+'_arwd', 'role_attr_flags':
      'NOSUPERUSER,INHERIT,NOCREATEDB,NOCREATEROLE,NOREPLICATION,NOLOGIN',
      'db': item.key, 'objs': 'public', 'type': 'schema', 'privs': 'USAGE'}] }}
    # this list grant SELECT/INSERT/UPDATE/DELETE to r_ prefixed roles
    postgresql_default_arwdr_list: >
      {{ postgresql_default_arwdr_list|
      default([]) + 
      [{'name': 'r_'+item.key+'_arwd', 'db': item.key, 'objs': 'TABLES',
      'privs': 'SELECT,INSERT,UPDATE,DELETE', 'type': 'default_privs', 'target_roles': item.key}] +
      [{'name': 'r_'+item.key+'_arwd', 'db': item.key, 'objs': 'SEQUENCES',
      'privs': 'SELECT,UPDATE', 'type': 'default_privs', 'target_roles': item.key}] }}
    # generate SELECT/INSERT/UPDATE/DELETE g_ prefixed roles list
    postgresql_arwdg_user_list: >
      {{ postgresql_arwdg_user_list|
      default([]) +
      [{'name': 'g_'+item.key+'_arwd', 'role_attr_flags':
      'NOSUPERUSER,INHERIT,NOCREATEDB,NOCREATEROLE,NOREPLICATION,NOLOGIN'}] }}
    # this list grant r_ prefixed privileges to g_ prefixed role
    postgresql_arwdg_privs_list: >
      {{ postgresql_arwdg_privs_list|
      default([]) +
      [{'db': item.key, 'objs': 'r_'+item.key+'_arwd', 'name': 'g_'+item.key+'_arwd', 'type': 'group'}] }}
  loop: "{{ postgresql_service_users | dict2items }}"

- name: PostgreSQL | Generate read only users list
  set_fact:
    # this list permit LOGIN to read only users
    postgresql_ro_users_list: >
      {{ postgresql_ro_users_list|
      default([]) +
      [{'name': item.key, 'password': item.value, 'role_attr_flags': 'LOGIN' }] }}
  loop: "{{ postgresql_ro_users | dict2items }}"
  when:
    - postgresql_ro_users is defined
    - postgresql_ro_users|length >0

- name: PostgreSQL | Generate ARWD users list
  set_fact:
    # this list permit LOGIN to arwd users
    postgresql_arwd_users_list: >
      {{ postgresql_arwd_users_list|
      default([]) +
      [{'name': item.key, 'password': item.value, 'role_attr_flags': 'LOGIN' }] }}
  loop: "{{ postgresql_arwd_users | dict2items }}"
  when:
    - postgresql_arwd_users is defined
    - postgresql_arwd_users|length >0

- name: PostgreSQL | generate privileges list for ro & arwd users
  set_fact:
    # this list grant SELECT to read only users
    postgresql_ro_privs_users_list: |
      {% set res = [] -%}
      {% for key in postgresql_ro_users.keys() -%}
        {% for db in postgresql_dbs_list -%}
        {% set somevar = res.extend([{'db': db, 'name': key, 'type': 'group', 'objs': 'g_'+db+'_ro'}]) -%}
        {%- endfor %}
      {%- endfor %}
      {{ res }}
    # this list grant SELECT/INSERT/UPDATE/DELETE to arwd users
    postgresql_arwd_privs_users_list: |
      {% set res = [] -%}
      {% for key in postgresql_arwd_users.keys() -%}
        {% for db in postgresql_dbs_list -%}
        {% set somevar = res.extend([{'db': db, 'name': key, 'type': 'group', 'objs': 'g_'+db+'_arwd'}]) -%}
        {%- endfor %}
      {%- endfor %}
      {{ res }}

# create users, databases & grant privileges by loop over lists created above
- name: PostgreSQL | Create users
  include_tasks: users.yml
  loop: >
    {{ postgresql_svc_users_list + 
    postgresql_ror_list + 
    postgresql_rog_user_list +
    postgresql_arwdr_list +
    postgresql_arwdg_user_list }}
  when: postgresql_svc_users_list|length > 0

- name: PostgreSQL | Create ro users
  include_tasks: users.yml
  loop: "{{ postgresql_ro_users_list }}"
  when: postgresql_ro_users_list is defined

- name: PostgreSQL | Create arwd users
  include_tasks: users.yml
  loop: "{{ postgresql_arwd_users_list }}"
  when: postgresql_arwd_users_list is defined

- name: PostgreSQL | Create databases
  include_tasks: databases.yml
  loop: "{{ postgresql_db_list }}"
  when: postgresql_db_list|length > 0

- name: PostgreSQL | Grant privileges
  include_tasks: privileges.yml
  loop: >
    {{ postgresql_svc_privileges_list + 
    postgresql_ror_list +
    postgresql_default_ror_list +
    postgresql_arwdr_list +
    postgresql_default_arwdr_list +
    postgresql_rog_privs_list +
    postgresql_arwdg_privs_list +
    postgresql_ro_privs_users_list +
    postgresql_arwd_privs_users_list }}
  when: postgresql_svc_privileges_list|length > 0 #and not ansible_check_mode
